﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetScript : MonoBehaviour
{
    
    private float bodyRadius;
    private Vector2 position;

    private void Start()
    {
        //sets bodyRadius
        float planetScale = gameObject.GetComponent<Transform>().localScale.x;
        CircleCollider2D[] colliders = new CircleCollider2D[1];
        gameObject.GetComponentInChildren<Rigidbody2D>().GetAttachedColliders(colliders);
        bodyRadius = colliders[0].radius;
        bodyRadius = planetScale * bodyRadius;

        //sets position
        position = new Vector2(transform.position.x, transform.position.y);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        //if the other object is a player, set the players gravityCenter to the planets transform and pass on the bodyRadius
        if(other.GetComponent<PlayerScript>() != null)
        {
            other.gameObject.GetComponent<PlayerScript>().SetGravityCenter(position);
            other.gameObject.GetComponent<PlayerScript>().SetPlanetRadius(bodyRadius);
        }
    }
}