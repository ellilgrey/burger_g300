﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalScript : MonoBehaviour
{
    public int goalNumber = 1;
    public UIController controller;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Ball"))
        {
            controller.Goal(goalNumber);
            collision.gameObject.GetComponent<BallScript>().Init();
        }
    }
}
