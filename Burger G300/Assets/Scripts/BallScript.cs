﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScript : MonoBehaviour
{

    private Rigidbody2D rb;
    private float speed;

    public float startSpeed = 5f;
    public float speedIncrement = 1f;
    public float acceleration = 20.0f;

    void Start()
    {

        //Initialize components
        rb = gameObject.GetComponent<Rigidbody2D>();

        //Create starting speed and direction
        Init();
    }

    void FixedUpdate()
    {
        if(rb.velocity.magnitude != speed)
        {
            rb.velocity = rb.velocity.normalized * speed;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            speed = speed + speedIncrement;
        }
    }

    public void Init()
    {
        rb.MovePosition(new Vector2(0,0));
        speed = startSpeed;
        //Create starting speed and direction
        Vector2 direction = Random.insideUnitCircle.normalized;
        rb.velocity = direction * speed;
    }
}
