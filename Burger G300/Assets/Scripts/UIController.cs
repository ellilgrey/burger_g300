﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    private int score1;
    private int score2;

    public Text text1;
    public Text text2;
    public int endScore = 5;
    public GameObject gameOverPanel;

    private bool paused;
    private bool escPressed;
    private bool gameOver;

    // Start is called before the first frame update
    private void Awake()
    {
        Time.timeScale = 1f;
    }
    void Start()
    {
        paused = false;
        gameOverPanel.SetActive(false);
        score1 = 0;
        score2 = 0;
        UpdateText();
        gameOver = false;
    }

    private void Update()
    {
        if(gameOver)
        {
            if(Input.GetAxisRaw("Submit") == 1)
            {
                SceneManager.LoadScene("MainMenu");
            }
        }
        if (Input.GetAxisRaw("Escape") == 1)
        {
            if (escPressed == false)
            {
                escPressed = true;
                if (paused == false)
                {
                    paused = true;
                    Time.timeScale = 0f;
                }
                else
                {
                    paused = false;
                    Time.timeScale = 1f;
                }
            }
        }
        else
        {
            escPressed = false;
        }
    }
    public void Goal(int player)
    {
        if(player == 1)
        {
            score2++;
        }
        else
        {
            score1++;
        }
        UpdateText();
        
        if(score1 == endScore || score2 == endScore)
        {
            EndGame();
        }
    }

    void UpdateText()
    {
        text1.text = "" + score1;
        text2.text = "" + score2;
    }

    void EndGame()
    {
        gameOverPanel.GetComponentInChildren<Text>().text = score1 + " GameOver " + score2;
        gameOverPanel.SetActive(true);
        Time.timeScale = 0f;

        gameOver = true;
    }
}
