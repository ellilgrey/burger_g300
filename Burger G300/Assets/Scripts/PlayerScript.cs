﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    private Vector2 gravityCenter = new Vector2(0,0);
    private float planetRadius;
    private Rigidbody2D rb;
    private Transform t;
    private Vector2 perpendicular;
    private Vector2 normal;
    private Vector2 direction;
    private float height;
    private float distance;
    private float MAX_JUMP_FORCE;
    private float MIN_JUMP_FORCE;
    private PlanetScript currentOrbitingPlanet;
    private bool onGround = false;
    private bool jumping = true;
    private bool axisPressed = false;
    private float jumpTime = 0.25f;
    private float jumpTimeLeft = 0.25f;

    public bool jump_remembered = false;
    public float min_jump_height = 1.0f;
    public float max_jump_height = 2.0f;
    public float jump_duration = 1.0f;
    public int playerNumber = 1;
    public float speed = 30f;
    public float jumpSpeed = 20f;
    public float GRAVITY = 10f;
    public float margin = .145f;

    

    

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        t = gameObject.GetComponent<Transform>();
        height = gameObject.GetComponent<BoxCollider2D>().size.y;

        //GRAVITY = 2 * max_jump_height / pow(jump_duration, 2)

        MAX_JUMP_FORCE = Mathf.Sqrt(2 * GRAVITY * max_jump_height);
        //Debug.Log("Max force: " + string.Format("{0:N2}", MAX_JUMP_FORCE));

        MIN_JUMP_FORCE = Mathf.Sqrt(2 * GRAVITY * min_jump_height);
        //Debug.Log("Min force: " + string.Format("{0:N2}", MIN_JUMP_FORCE));
    }
    void FixedUpdate()
    {
        if (Input.GetAxisRaw("Vertical" + playerNumber) == 1) {axisPressed = true;}
        if (Input.GetAxisRaw("Vertical" + playerNumber) == 0) { axisPressed = false;}
        Gravity();
        PlayerMove();
    }
    public void SetNewPlanetoid(PlanetScript planetoid) 
    {
        currentOrbitingPlanet = planetoid;
    }
    public void SetGravityCenter(Vector2 center)
    {
        gravityCenter = center;
        jumping = false;
        jumpTimeLeft = jumpTime;
    }
    public void SetPlanetRadius(float r)
    {
        planetRadius = r;
    }
    private void Gravity()
    {
        direction = new Vector2(t.position.x, t.position.y);
        direction = gravityCenter - direction;
        distance = direction.magnitude;
        direction.Normalize();
        perpendicular = Vector2.Perpendicular(direction);

        rb.AddForce(direction * GRAVITY);
    }
    private void PlayerMove()
    {
        //move perpendicular to the gravity vector when there is horizontal input
        perpendicular = perpendicular * Input.GetAxis("Horizontal" + playerNumber) * speed;

        rb.AddForce(perpendicular);

        Jump();
    }
    private void Jump()
    {

        if (Input.GetAxisRaw("Vertical" + playerNumber) == 0 && jumping == true)
        {
            //Debug.Log("Input Released");
            jumping = false;
            rb.velocity = new Vector2(0.0f, 0.0f);
            jumpTimeLeft = jumpTime;           
        }

        if (Input.GetAxisRaw("Vertical" + playerNumber) == 1 && onGround)
        {
            //Debug.Log("Jump Input Recieved");
            rb.AddForce(direction * -MAX_JUMP_FORCE * 20);
            jumping = true;
        }

        if (Input.GetAxisRaw("Vertical" + playerNumber) == 1 && jumping == true)
        {
            rb.AddForce(direction * -MAX_JUMP_FORCE * 20);
            jumpTimeLeft -= Time.deltaTime;
        }

        if (jumpTimeLeft <= 0.0 && jumping == true)
        {
            jumping = false;
            jumpTimeLeft = jumpTime;
            rb.velocity = new Vector2(0.0f, 0.0f);
        }
    }


    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.CompareTag("Planetoid"))
        {
            onGround = true;
            jumping = false;
        }
    }

    void OnCollisionExit2D(Collision2D other)
    {
        if (other.collider.CompareTag("Planetoid"))
        {
            onGround = false;
        }
    }
}


